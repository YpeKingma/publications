# publications

For best quality viewing, download a pdf and use a local pdf viewer.

Licensed under CC BY-SA 4.0, see the [LICENSE](https://gitlab.com/YpeKingma/publications/-/blob/master/LICENSE) file.

## "The key role for adding meter values while maintaining privacy"

30 March 2020

*  [English](https://gitlab.com/YpeKingma/publications/-/blob/master/meterpriv/Kingma2020MeterPrivEN.pdf),
*  [Dutch](https://gitlab.com/YpeKingma/publications/-/blob/master/meterpriv/Kingma2020MeterPrivNL.pdf).

Summary presentations for this article:

*  [English](https://gitlab.com/YpeKingma/publications/-/blob/master/meterpriv/Kingma2020MeterPrivBeamerEN.pdf),
*  [Dutch](https://gitlab.com/YpeKingma/publications/-/blob/master/meterpriv/Kingma2020MeterPrivBeamerNL.pdf).



